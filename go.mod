module github.com/moov-io/base

go 1.13

require (
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/go-kit/kit v0.9.0
	github.com/gorilla/mux v1.7.3
	github.com/hashicorp/golang-lru v0.5.4
	github.com/prometheus/client_golang v1.4.0
	github.com/rickar/cal v1.0.1
)
